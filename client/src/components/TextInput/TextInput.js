export default {
  name: 'TextInput',
  props: {
    fieldName: String,
    fieldTitle: String,
    fieldType: String,
    placeholder: String,
    fieldValue: String,
    fieldChange: Function
  },
  data () {
    return {
      field: this.fieldValue
    }
  },
  methods: {
    setText (value) {
      this.fieldChange(value, this.fieldName)
    }
  }
}
