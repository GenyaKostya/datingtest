import {getAllUsers, getUserById, createUser, banUser, getAllUsersBan, deactiveUser, updateAvatar} from '../../api/rest/usersService'
import {USERS,
  USERS_ERROR,
  USERS_REQUEST,
  USERS_RESPONSE,
  USER_BY_ID,
  GET_USER_BY_ID,
  SINGLE_USER_RESPONSE,
  CREATE_USER,
  CREATE_USER_RESPONSE,
  USER_ERROR_BAN,
  USER_RESPONSE_BAN,
  USER_BAN,
  USER_REQUEST_BAN,
  GET_USERS_BAN,
  USER_ERROR_ACTIVE,
  USER_ACTIVE,
  USER_RESPONSE_ACTIVE,
  UPDATE_USER_RESPONSE_AVATAR,
  UPDATE_USER_ERROR_AVATAR,
  UPDATE_USER_AVATAR
} from '../constants'
export default ({
  state: {
    message: null,
    users: [],
    isFetching: false,
    error: null
  },
  mutations: {
    [USERS_REQUEST] (state) {
      state.isFetching = true
    },
    [USER_REQUEST_BAN] (state) {
      state.isFetching = true
    },
    [USERS_RESPONSE] (state, users) {
      state.users = users;
      state.isFetching = false;
      state.error = null
    },
    [USERS_ERROR] (state, error) {
      state.error = error;
      state.isFetching = false
    },
    [SINGLE_USER_RESPONSE] (state, user) {
      const userIndex = state.users.findIndex(u=> u.id === user.id);
      if (userIndex === -1) {
        state.users.push(user)
      } else {
        state.users[userIndex] = { ...state.users[userIndex], ...user }
      }
      state.isFetching = false;
      state.error = null
    },
    [CREATE_USER_RESPONSE] (state, data){
      state.message = data.message;
      state.isFetching = false;
      state.error = null
      },
    [USER_RESPONSE_BAN] (state, data) {
      state.isFetching = false;
      state.error = null;
      const userIndex = state.users.findIndex(u => u.id === data.id)
      if (userIndex === -1) {
        state.users.push(data)
      } else {
        state.users[userIndex] = { ...state.users[userIndex], ...data }
      }

    },
    [USER_ERROR_BAN] (state, error) {
      state.error = error;
      state.isFetching = false
    },
    [USER_RESPONSE_ACTIVE] (state, data) {
      const userIndex = state.users.findIndex(u => u.id === data.id)
      if (userIndex === -1) {
        state.users.push(data)
      } else {
        state.users[userIndex] = { ...state.users[userIndex], ...data }
      }
      state.isFetching = false;
      state.error = null
    },
    [USER_ERROR_ACTIVE] (state, error) {
      state.error = error;
      state.isFetching = false
    },
    [UPDATE_USER_RESPONSE_AVATAR] (state, user) {
      const userIndex = state.users.findIndex(u => u.id === user.id)
      if (userIndex === -1) {
        state.users.push(user)
      } else {
        state.users[userIndex] = { ...state.users[userIndex], ...user }
      }
      state.isFetching = false
      state.error = null
    },
    [UPDATE_USER_ERROR_AVATAR] (state, error) {
      state.error = error
      state.isFetching = false
    },

  },
  actions: {
    async [USERS]({commit}) {
      commit(USERS_REQUEST);
      try {
        const {data} = await getAllUsers();

        commit(USERS_RESPONSE, data)
      } catch (e) {
        commit(USERS_ERROR, e)
      }
    },
    async [GET_USERS_BAN]({commit}) {
      commit(USERS_REQUEST);
      try {
        const {data} = await getAllUsersBan();
        commit(USERS_RESPONSE, data)
      } catch (e) {
        commit(USERS_ERROR, e)
      }
    },
    async [USER_BY_ID] ({ commit }, id) {
      commit(USERS_REQUEST);
      try {
        const { data } = await getUserById(id);
        commit(SINGLE_USER_RESPONSE, data)
      } catch (e) {
        commit(USERS_ERROR, e)
      }
    },
    async [CREATE_USER] ({ commit }, createData) {
      commit(USERS_REQUEST);
      try {
        const { data } = await createUser(createData);
        commit(CREATE_USER_RESPONSE, data)
      } catch (e) {
        commit(USERS_ERROR, e)
      }
    },
    async [USER_BAN] ({ commit }, user) {
      commit(USER_REQUEST_BAN);
      try {
        const { data } = await banUser(user);
        commit(USER_RESPONSE_BAN, data)
      } catch (e) {
        commit(USER_ERROR_BAN, e)
      }
    },
    async [USER_ACTIVE] ({ commit }, user) {
      commit(USERS_REQUEST);
      try {
        const { data } = await deactiveUser(user);
        commit(USER_RESPONSE_ACTIVE, data)
      } catch (e) {
        commit(USER_ERROR_ACTIVE, e)
      }
    },
    async [UPDATE_USER_AVATAR] ({ commit }, fileid) {
      commit(USERS_REQUEST);
      try {
        const { data } = await updateAvatar(fileid);
        commit(UPDATE_USER_RESPONSE_AVATAR, data)
      } catch (e) {
        commit(UPDATE_USER_ERROR_AVATAR, e)
      }
    }

  },
  getters: {
    [GET_USER_BY_ID]: state => id => state.users.find(u => u.id === parseInt(id))
  }
})
