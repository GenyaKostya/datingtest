import {getPhoto, inputPhoto, deletePhoto} from '../../api/rest/photoService.js'
import {
  GET_USER_PHOTO,
  GET_USER_PHOTO_RESPONSE,
  GET_USER_PHOTO_ERROR,
  PHOTO_REQUEST,
  GET_USER_PHOTO_BYID,
  ADD_USER_PHOTO,
  ADD_USER_PHOTO_RESPONSE,
} from '../constants'

export default ({
  state: {
    photos: [],
    isFetching: false,
    error: null,
    message: null
  },
  mutations: {
    [PHOTO_REQUEST](state) {
      state.isFetching = true
    },
    [GET_USER_PHOTO_RESPONSE](state, data) {
      state.photos = data;
      state.isFetching = false;
      state.error = null
    },
    [GET_USER_PHOTO_ERROR](state, error) {
      state.error = error;
      state.isFetching = false
    },
    [ADD_USER_PHOTO_RESPONSE](state, data) {
      state.isFetching = false;
      state.error = null;
      state.message = data.message
    },
  },
    actions: {
      async [GET_USER_PHOTO]({commit}, id) {
        commit(PHOTO_REQUEST);
        try {
          const {data} = await getPhoto(id);
          commit(GET_USER_PHOTO_RESPONSE, data)
        } catch (e) {
          commit(GET_USER_PHOTO_ERROR, e)
        }
      },
      async [ADD_USER_PHOTO]({commit}, id) {
        commit(PHOTO_REQUEST);
        try {
          const {data} = await inputPhoto(id);
          commit(ADD_USER_PHOTO_RESPONSE, data)
        } catch (e) {
          commit(GET_USER_PHOTO_ERROR, e)
        }
      }
    },
    getters: {
      [GET_USER_PHOTO_BYID]: state => id => state.photos.find(u => u.id === id)
    }
  })
