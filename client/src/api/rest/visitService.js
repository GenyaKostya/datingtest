import { restURL } from '../baseURL'
import axios from 'axios'


export const visitUser = (id) => axios.get(restURL + '/visit/'  + id, {headers: { Authorization: `Bearer ${localStorage.getItem('access')} `}});

export const getVisitors = () => axios.get(restURL + '/visit', {headers: { Authorization: `Bearer ${localStorage.getItem('access')} `}});

