import { restURL } from '../baseURL'
import axios from 'axios'

export const getPhoto = (uid) => axios.get(restURL + '/photo/' + uid, {headers: { Authorization: `Bearer ${localStorage.getItem('access')} `}})

export const inputPhoto = (data) => {
  const formData = new FormData;
  formData.append('file', data.photoName);
  return axios.post(restURL + '/photo/' + data.id, formData, {headers: {Authorization: `Bearer ${localStorage.getItem('access')} `, 'Content-Type': 'multipart/form-data' }})
};
