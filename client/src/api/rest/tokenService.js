import { restURL } from '../baseURL'
import axios from 'axios'


export const logIn = (loginData) => axios.post(restURL + '/login', loginData );

export const logout = () => axios.post(restURL + '/logout', {refreshToken: localStorage.getItem('refresh')});

export const authUser = () => axios.get(restURL + '/auth', {headers: { Authorization: `Bearer ${localStorage.getItem('access')} `}});
