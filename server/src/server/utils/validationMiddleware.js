const yup = require('yup');
const moment = require('moment');
const schema = yup.object().shape({
    firstName: yup.string().trim(),
    lastName: yup.string().trim(),
    age: yup
        .number()
        .test(
            'age',
            'You must be over 18 years old',
            (value) => moment(moment().subtract(18, 'years').unix()).diff(value) >= 0
        ),
    password: yup.string().trim().min(8),
    role: yup.number().min(0).max(2),
    intention: yup.string(),
    status: yup.string(),
    gender: yup.string(),
});
/**
 * function check for valid input data for update user
 */
module.exports = function(req,res,next ) {
    schema.isValid(req.body)
        .then(isValid => {
            if(isValid) {
                next();
            } else {
                next({code:400, message:"Input data not correct"})
            }
        });
};
