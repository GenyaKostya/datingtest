import "babel-polyfill";

import multer from 'multer';
import bcrypt from 'bcrypt'

import {User} from '../models/index'
import {Token} from '../models/index'
import {Visitor} from '../models/index'

const jwt = require('jsonwebtoken');


const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports.createUser = (req, res,next) => {
    const user = new User(req.body);
    if(user.role !== 0) {
        res.send("Can't create not user");
    }
    user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8));
    user.save()
        .then(user => {
            if(!user) {
                throw ("Can't create user!!!");
            }
            res.send({message:'Success Create'});
        })
        .catch(err => next({code: 400, message: err.message}))
};

module.exports.getUserById = async (req, res) => {
    try {
        const result = await User.findOne({where: {id: req.params.id}});
        if (!result) throw ("User not find");
        res.send(result)
    } catch (e) {
        res.send("User not find")
    }
};
const role = {
    ADMIN: 2,
    USER: 0,
    VIP: 1
};

module.exports.updateUser = (req, res, next) => {
    const updatedUser = req.body;
    const id = req.params.id;
    if (!req.headers.authorization && req.headers.authorization.split(' ')[0] !== 'Bearer') {
        return next({status: 401, message: 'Access not find'})
    }
    const accessToken = req.headers.authorization.split(' ')[1];
    const decodeAccess = jwt.verify(accessToken, 'secret_key');
    updatedUser.password = bcrypt.hashSync(updatedUser.password, bcrypt.genSaltSync(8));
    if (decodeAccess.id !== id && decodeAccess.role !== role.ADMIN) {
        next("You have no root");
        return;
    }
    User.update(req.body, {
        where: {id: req.params.id}, returning: true,
        attributes: !(decodeAccess.role === role.ADMIN && decodeAccess.id !== id && updatedUser.role < role.ADMIN) ? {exclude: ['isBaned', 'role', 'email', 'profilePicture', 'id']} : {}
    })
        .then(user => {
            if (!user[0]) {
                throw ("User not exist!!!");
            }
            res.send(user[1][0]);
        })
        .catch(err => next({code: 400, message: err}));
};

module.exports.deactiveUser =  (req, res, next) => {
    const accessToken = req.headers.authorization.split(' ')[1];
    const decodeAccess = jwt.verify(accessToken,'secret_key');
    const id = req.params.id;

    if(decodeAccess.id == id || decodeAccess.role === role.USER) {
        next("You have no licence");
        return;
    }
    const deactiveUser = req.body;
    if(decodeAccess.role === role.ADMIN && decodeAccess.id !== id && deactiveUser.role < decodeAccess.role) {
        User.update({isActive: false}, {where: {id}, returning: true})
            .then(user => {
                if (!user[0]) {
                    throw ("User not exist!!!");
                }
                res.send(user[1][0]);
            })
            .catch(err => next({code: 400, message: err}))
    }else {
        User.findById(id)
            .then(user => {
                if(!user) {
                    throw ("User not exist!!!");
                }
                res.send(user);
            })
            .catch(err => next({code: 400, message: err}))
    }
};
module.exports.banUser =  (req, res, next) => {
    const accessToken = req.headers.authorization.split(' ')[1];
    const decodeAccess = jwt.verify(accessToken,'secret_key');
    const id = req.params.id;

    if(decodeAccess.id == id || decodeAccess.role === role.USER) {
        next("You can't");
        return;
    }
    console.log(req.body)
    const banedUser = req.body;
    if(decodeAccess.role > role.USER && decodeAccess.id !== id && banedUser.role < decodeAccess.role) {
        User.update({isBaned: !banedUser.isBaned}, {where: {id}, returning: true})
            .then(user => {
                if (!user[0]) {
                    throw ("User not exist!!!");
                }
                res.send(user[1][0]);
            })
            .catch(err => next({code: 400, message: err}))
    }else {
        User.findById(id)
            .then(user => {
                if(!user) {
                    throw ("User not exist!!!");
                }
                res.send(user);
            })
            .catch(err => next({code: 400, message: err}))
    }
};



module.exports.getAllUsersBan = async (req, res, next) => {
    User.findAll()
        .then(user => {
            res.send(user);
        })
        .catch(err => next({code: 400, message: err}))
};

module.exports.AllUsers = (req, res, next) => {
    const que = req.query;
    let result = {};
    const Input = [];
    Input.push({isBaned: 'false', isActive: 'true'});
    if (que.name) {
        let name = req.query.name.trim();
        name = name.split(' ');
        let changedName = '';
        for (let key in name) {
            changedName += `%${name[key]}%`
        }
        Input.push({
            [Op.or]:
                [
                    Sequelize.where(
                        Sequelize.fn(
                            'concat',
                            Sequelize.col('firstName'),
                            ' ',
                            Sequelize.col('lastName')
                        ), {[Op.iLike]: changedName}),
                    Sequelize.where(
                        Sequelize.fn(
                            'concat',
                            Sequelize.col('lastName'),
                            ' ',
                            Sequelize.col('firstName')
                        ), {[Op.iLike]: changedName})
                ]
        })
    }
    if (que.firstName) {
        Input.push({firstName: {[Op.iLike]: `%${que.firstName}%`}});
    }
    if (que.lastName) {
        Input.push({lastName: {[Op.iLike]: `%${que.lastName}%`}});
    }
    if (que.gender) {
        Input.push({gender: {[Op.like]: req.query.gender}})
    }
    if (que.intention) {
        Input.push({intention: {[Op.like]: req.query.intention}})
    }
    if (que.profilePicture) {
        Input.push({profilePicture: {[Op.ne]: null}})
    }
    if (que.ageBefore && que.ageAfter) {
        Input.push({
            age: {
                [Op.between]: [req.query.ageBefore, req.query.ageAfter]
            }
        })
    }
    result[Op.and] = Input;
    User.findAll({where: result})
        .then(user => {
            res.send(user);
        })
        .catch(err => next({code: 400, message: err}))
};
module.exports.updateAvatar = (req, res, next) => {
    const id = req.params.id;
    let filename = "";
    const storage = multer.diskStorage({
        destination: './public/img/',
        filename: (req, files, cb) => {
            filename = Date.now() + '-' + files.originalname;
            cb(null, filename);
        }
    });
    const upload = multer({
        storage: storage
    }).any();

    upload(req, res, function (err) {
        if (err) {
            return next(err);
        } else {
            User.update({profilePicture: filename}, {where: {id: id}});
            res.send("Success");
        }
    });
};
module.exports.visitUser = async (req, res, next) => {
    const accessToken = req.headers.authorization.split(' ')[1];
    const decodeAccess = jwt.verify(accessToken,'secret_key');

    const visitorId = decodeAccess.id;
    const visitedId = req.params.id;
    try {
        await Visitor.destroy({where: {userId: visitedId, visitorId: visitorId}});
        if (visitorId !== visitedId) {
            await Visitor.create({userId: visitedId, visitorId: visitorId});
        }
        res.send('Success');
    } catch (e) {
        next({code: 500, message: 'Server error'});
    }
};

module.exports.getVisitors = async (req, res, next) => {
    const accessToken = req.headers.authorization.split(' ')[1];
    const decodeAccess = jwt.verify(accessToken,'secret_key');
    const userId = decodeAccess.id;
    try {
        const visitors = await Visitor.findAll({
            where: {visitorId: userId}
        });
        res.send(visitors);
    } catch (e) {
        console.log(e);
        next({code: 500, message: 'Server error'});
    }
};

