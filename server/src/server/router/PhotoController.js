import "babel-polyfill";
import {Photo, User} from '../models/index';
import multer from 'multer'


module.exports.addPhoto = (req, res,next) => {
    const id = req.params.id;
    console.log('21321')
    User.findByPk(id)
        .then(user => {
            if(!user) {
                throw ("User not exist!!!");
            }
            let filename = "";

            const storage = multer.diskStorage({
                destination: './public/img/',

                filename: (req, files, cb) => {
                    filename = Date.now() + '-' + files.originalname;
                    cb(null, filename);
                }
            });
            const upload = multer({
                storage: storage,
                fileFilter: (req, file, cb) => {
                    if(file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg') {

                        cb(null, true);
                    }
                    else {
                        next({code: 400, message: 'Only images allowed'})
                    }
                },
            }).any();
            upload(req, res, async function (err) {
                if (err) {
                    return next(err);

                } else {
                    console.log(filename, id);
                    Photo.create({'photoName': filename,'userId' : id});
                    res.send({message: "Success add photo"});
                }
            })
        })
        .catch(err => next({code: 400, message: err}))
};

module.exports.getPhoto = (req, res, next) => {
    const id = req.params.id;
    User.findById(id)
        .then(user => {
            if (!user) {
                throw ("User not exist!!!");
            }
            Photo.findAll({where: {userId: id}})
                .then(photo => {
                    if (!photo) {
                        throw ("Any photo found!!!");
                    }
                    res.send(photo);
                })
        })
        .catch(err => next({code: 400, message: err}))
};
module.exports.deletePhoto = (req, res, next) => {
    const id = req.params.id;
    const name = req.body.photoName;
    Photo.destroy({where: {userId: id, photoName: name}});
    res.send("success")
}

