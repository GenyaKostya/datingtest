import express from 'express';
import "babel-polyfill";

const UserController = require('./UserController');
const roleValidation = require('../utils/roleValidation');
const TokenController = require('./TokenController');
const accessValid = require('../utils/accessValid');
const PhotoController = require('./PhotoController');
const validationMiddleware = require('../utils/validationMiddleware');

const router = express.Router();

router.get('/api/ban', accessValid, roleValidation, UserController.getAllUsersBan);
router.get('/api/profile/:id', accessValid, UserController.getUserById);
router.post('/api/user', validationMiddleware, UserController.createUser);
router.put('/api/user/ban/:id', accessValid, roleValidation, UserController.banUser);
router.put('/api/user/active/:id', accessValid, roleValidation, UserController.deactiveUser);

router.put('/api/user/:id', UserController.updateUser);
router.get('/api/AllUsers', accessValid, UserController.AllUsers);

router.post('/api/avatar/:id', UserController.updateAvatar);

router.post('/api/photo/:id', accessValid, PhotoController.addPhoto);
router.get('/api/photo/:id', accessValid, PhotoController.getPhoto);
router.delete('/api/photo/:id', accessValid, PhotoController.deletePhoto);

router.post('/api/refresh', TokenController.tokenRefresh);

router.post('/api/login', TokenController.login);
router.post('/api/logout', TokenController.logout);

router.get('/api/auth', accessValid, TokenController.authByToken);

router.get('/api/visit/:id', UserController.visitUser);
router.get('/api/visit', UserController.getVisitors);
module.exports = router;
